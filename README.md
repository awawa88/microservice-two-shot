# Wardrobify

Team:

* Person 1 - Andrew Lam -Hat
* Person 2 - Daniel Hernandez-Shoes microservice

## Design
├── db
│   └── create-multiple-databases.sh
├── docker-compose.yml
├── ghi
│   └── app
│       ├── node_modules
│       ├── package.json
│       ├── package-lock.json
│       ├── public
│       ├── README.md
│       ├── run.sh
│       ├── src
│       └── windows-setup.js
├── hats
│   ├── api
│   │   ├── common
│   │   ├── Dockerfile.dev
│   │   ├── hats_project
│   │   ├── hats_rest
│   │   ├── manage.py
│   │   └── requirements.txt
│   └── poll
│       ├── Dockerfile.dev
│       ├── hats_project
│       ├── hats_rest
│       ├── poller.py
│       └── requirements.txt
├── package-lock.json
├── README.md
├── shoes
│   ├── api
│   │   ├── common
│   │   ├── Dockerfile.dev
│   │   ├── manage.py
│   │   ├── requirements.txt
│   │   ├── shoes_project
│   │   └── shoes_rest
│   └── poll
│       ├── Dockerfile.dev
│       ├── poller.py
│       ├── requirements.txt
│       ├── shoes_project
│       └── shoes_rest
└── wardrobe
    └── api
        ├── common
        ├── Dockerfile.dev
        ├── manage.py
        ├── requirements.txt
        ├── wardrobe_api
        └── wardrobe_project


## Shoes microservice

Shoes Microservice
Access the Django shoe API application from your browser or Insomnia on port 8080. The Shoe function should track the manufacturer, model Name color, picture, and bin Location in the wardrobe where it exists. This React component shows a list of all shoes in the sql database. This React component allows you to create and delete shoes in the database. All the links for the shoes are correctly routed to the appropriate code.

## Hats microservice

Similar to shoes microservice. but calling http://localhost:8090/api/hats instead to create hat and give hat with some attributes.
