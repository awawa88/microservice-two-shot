import React, { useEffect, useState, useMemo } from 'react'

function ShoeForm({ getShoes }) {
	const [shoeBrand, setShoeBrand] = useState('')
	const [shoeName, setShoeName] = useState('')
	const [shoeColor, setShoeColor] = useState('')
	const [shoePictureUrl, setShoePictureUrl] = useState('')
	const [bins, setBins] = useState([])
	const [bin, setBin] = useState('')

	const handleShoeBrandChange = (event) => {
		const value = event.target.value
		setShoeBrand(value)
	}

	const handleShoeNameChange = (event) => {
		const value = event.target.value
		setShoeName(value)
	}

	const handleShoeColorChange = (event) => {
		const value = event.target.value
		setShoeColor(value)
	}

	const handleShoePictureUrlChange = (event) => {
		const value = event.target.value
		setShoePictureUrl(value)
	}

	const handleBinChange = (event) => {
		const value = event.target.value
		setBin(value)
	}

	const handleSubmit = async (event) => {
		event.preventDefault()
		console.log('???')
		const data = {}

		data.shoe_brand = shoeBrand
		data.shoe_name = shoeName
		data.shoe_color = shoeColor
		data.shoe_picture_url = shoePictureUrl
		data.bin = bin

		const shoeUrl = 'http://localhost:8080/api/shoes/'
		const fetchConfig = {
			method: 'post',
			body: JSON.stringify(data),
			headers: {
				'Content-Type': 'application/json',
			},
		}

		const response = await fetch(shoeUrl, fetchConfig)

		if (response.ok) {
			console.log('printing okay')
			const createShoe = await response.json()
			console.log(createShoe)
			setShoeBrand('')
			setShoeName('')
			setShoeColor('')
			setShoePictureUrl('')
<<<<<<< HEAD
			setBin([])
=======
			setBin('')
			getShoes()
>>>>>>> 3ca3ec81e7face58b8c44cf60790b56ac07d28ba
			document.getElementById('bin').selectedIndex = 0
		}
	}

	const fetchData = async () => {
		const url = 'http://localhost:8100/api/bins/'
		const response = await fetch(url)

		if (response.ok) {
			const data = await response.json();
			console.log(data);
			setBins(data.bins);
		}
	}

	useEffect(() => {
		// hook
		fetchData() // fetches the data
	}, []) // means its only ran once

	return (
		<div className="row">
			<div className="offset-3 col-6">
				<div className="shadow p-4 mt-4">
					<h1>Create a shoe</h1>
					<form
						onSubmit={handleSubmit}
						id="create-shoe-form">
						<div className="form-floating mb-3">
							<input
								onChange={handleShoeBrandChange}
								placeholder="Shoe Brand"
								required
								type="text"
								name="shoe_brand"
								id="shoe_brand"
								className="form-control"
								value={shoeBrand}
							/>
							<label htmlFor="shoe_brand">Manufacturer</label>
						</div>
						<div className="form-floating mb-3">
							<input
								onChange={handleShoeNameChange}
								placeholder="Shoe Name"
								required
								type="text"
								name="shoe_name"
								id="shoe_name"
								className="form-control"
								value={shoeName}
							/>
							<label htmlFor="shoe_name">Model</label>
						</div>
						<div className="form-floating mb-3">
							<input
								onChange={handleShoeColorChange}
								placeholder="Shoe Color"
								required
								type="text"
								name="shoe_color"
								id="shoe_color"
								className="form-control"
								value={shoeColor}
							/>
							<label htmlFor="shoe_color">Shoe Color</label>
						</div>
						<div className="form-floating mb-3">
							<input
								onChange={handleShoePictureUrlChange}
								placeholder="Shoe Picture URL"
								required
								type="text"
								name="shoe_picture_url"
								id="shoe_picture_url"
								className="form-control"
								value={shoePictureUrl}
							/>
							<label htmlFor="shoe_picture_url">
								Picture URL
							</label>
						</div>
						<div className="mb-3">
							<select
								onChange={handleBinChange}
								required
								name="bin"
								id="bin"
								className="form-select">
								<option value="">Choose a Bin</option>
								{bins.map((bin) => {
									return (
										<option
											key={bin.id}
											value={bin.href}>
											{bin.closet_name}
										</option>
									)
								})}
							</select>
						</div>
						<button className="btn btn-primary">Create</button>
					</form>
				</div>
			</div>
		</div>
	)
}

export default ShoeForm
