import React, { useEffect, useState } from "react";

function ShoeLists({ shoes, getShoes }) {
	const deleteShoe = async (shoes) => {
		const shouldDelete = window.confirm(
			"Are you sure you want to delete " + shoes.style_name + "?"
		);
		if (!shouldDelete) {
			return;
		}

		const shoeUrl = `http://localhost:8080/api/shoes/${shoes.id}`;
		const fetchConfig = {
			method: "DELETE",
		};
		const response = await fetch(shoeUrl, fetchConfig);
		if (response.ok) {
			getShoes();
		}
	};
	console.log(shoes);
	return (
		<table className="table table-striped">
			<thead>
				<tr>
					<th>Manufactur</th>
					<th></th>
					<th></th>
				</tr>
			</thead>
			<tbody>
				{shoes.map((shoe) => {
					return (
						<tr key={shoe.id}>
							<td>{shoe.shoe_brand}</td>
							<td>
								<img
									src={shoe.shoe_picture_url}
									alt=""
									width="80px"
									height="80px"
								/>
							</td>
							<td>
								<button
									id={shoe.id}
									className="btn btn-danger"
									onClick={() => deleteShoe(shoe)}
									type="button">
									Delete
								</button>
							</td>
						</tr>
					)
				})}
			</tbody>
		</table>
	)
}

export default ShoeLists;
