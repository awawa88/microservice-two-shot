import React, { useEffect, useState } from 'react';

function HatLists({ hats, getHats }) {
  const deleteHat = async (hat) => {
    const shouldDelete = window.confirm(`Are you sure you want to delete the item "${hat.style_name}" ?`);
    if (!shouldDelete) {
      return;
    }

    const hatUrl = `http://localhost:8090/api/hats/${hat.id}`;
    const fetchConfig = {
      method: 'DELETE',
    };
    const response = await fetch(hatUrl, fetchConfig);
    if (response.ok) {
      getHats();
    }
  };

  return (
    <table className="table table-striped">
      <thead>
        <tr>
          <th>Style Name</th>
          <th></th>
          <th></th>
        </tr>
      </thead>
      <tbody>
        {hats.map((hat) => {
          return (
            <tr key={hat.id}>
              <td>{hat.style_name}</td>
              <td>
                <img src={hat.picture_url} alt="" width="80px" height="80px" />
              </td>
              <td>
                <button
                  id={hat.id}
                  className="btn btn-danger"
                  onClick={() => deleteHat(hat)}
                  type="button"
                >
                  Delete
                </button>
              </td>
            </tr>
          );
        })}
      </tbody>
    </table>
  );
}

export default HatLists;
