import { BrowserRouter, Routes, Route } from 'react-router-dom';
import { useEffect, useState } from 'react';
import MainPage from './MainPage';
import HatLists from './HatLists';
import ShoeLists from "./ShoeLists";
import ShoeForm from "./ShoeForm";
import HatForm from "./HatForm";
import Nav from "./Nav";

function App() {
	const [hats, setHats] = useState([]);

	const getHats = async () => {
		const hatsUrl = "http://localhost:8090/api/hats";

		const hatsResponse = await fetch(hatsUrl);

		if (hatsResponse.ok) {
			const data = await hatsResponse.json();
			const hats = data.hats;
			setHats(hats);
		}
	};

	const [shoes, setShoes] = useState([]);

	const getShoes = async () => {
		const shoesUrl = "http://localhost:8080/api/shoes";

		const shoesResponse = await fetch(shoesUrl);

		if (shoesResponse.ok) {
			const data = await shoesResponse.json();
			const shoes = data.shoes;
			setShoes(shoes);
		}
	};

  useEffect(() => {getHats(); getShoes()}, [setHats, setShoes]);
	return (
		<BrowserRouter>
			<Nav />
			<div className="container">
				<Routes>
					<Route path="/" element={<MainPage />} />
					<Route path="/hats" element={<HatLists hats={hats} getHats={getHats} />} />
          <Route path="hats/new" element={<HatForm getHats={getHats} />} />
					<Route path="/shoes" element={<ShoeLists shoes={shoes} getShoes={getShoes} />}/>
					<Route path="shoes/new" element={<ShoeForm getShoes={getShoes} />} />
				</Routes>
			</div>
		</BrowserRouter>
	);
}

export default App;
