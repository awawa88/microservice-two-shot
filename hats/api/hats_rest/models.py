from django.db import models
from django.urls import reverse

# Create your models here.


class LocationVO(models.Model):
    closet_name = models.CharField(max_length=100)
    section_number = models.PositiveBigIntegerField()
    shelf_number = models.PositiveSmallIntegerField()
    import_href = models.CharField(max_length=200, unique=True)


class Hat(models.Model):
    fabric = models.CharField(max_length=200)
    style_name = models.CharField(max_length=200)
    color = models.CharField(max_length=200)
    picture_url = models.URLField(null=True)
    location = models.ForeignKey(
        LocationVO,
        related_name="hats",
        on_delete=models.CASCADE,
    )

    def get_api_url(self):
        return reverse("api_hats", kwargs={"pk": self.pk})

    def __str__(self):
        return f"{self.style_name} - {self.style_name}/{self.color}"

    class Meta:
        ordering = ("style_name", "fabric", "color")
