from django.db import models
from django.urls import reverse


class BinVO(models.Model):
    closet_name = models.CharField(max_length=150)
    bin_number = models.PositiveIntegerField(null=True)
    bin_size = models.PositiveIntegerField(null=True)
    import_href = models.CharField(max_length=200, null=True)


class Shoe(models.Model):
    shoe_brand = models.CharField(max_length=200)
    shoe_name = models.CharField(max_length=200)
    shoe_color = models.CharField(max_length=200)
    shoe_picture_url = models.URLField(null=True)
    bin = models.ForeignKey(
        BinVO, related_name="shoes", null=True, on_delete=models.CASCADE
    )

    def get_api_url(self):
        return reverse("api_shoes", kwargs={"pk": self.pk})

    def __str__(self):
        return f"{self.shoe_brand} - {self.shoe_name}/{self.shoe_color}"

    class Meta:
        ordering = ("shoe_name", "shoe_brand", "shoe_color")
